#!/bin/bash

npm cache clean
./node_modules/.bin/bower cache clean

rm -rf npm-debug.log
rm -rf _dist
rm -rf node_modules
rm -rf bower_components

npm install
./node_modules/.bin/bower install
