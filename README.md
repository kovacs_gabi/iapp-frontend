Dependencies
==
Make sure you have Node js installed.  
Get it from here: https://nodejs.org/en/download/

Install project
==
1. First Git clone project to a local folder
`> git clone git@bitbucket.org:kovacs_gabi/iapp-frontend.git`

2. Run `> ./script/install.sh` to install all dependencies.

Run local server
==

After Install you run & view the website using this command:  

`> ./script/server.sh`
