// Globals
var gulp           = require('gulp');

var $              = require('gulp-load-plugins')();
var browserSync    = require('browser-sync');
var mainBowerFiles = require('main-bower-files');
var args           = require('get-gulp-args')();
var del            = require('del');
var lazypipe       = require('lazypipe');

var isProductionMode = args.env === 'production';

// BASE CONSTANTS
BASE_DIR        = "./app";
SERVE_DIR       = "./_dist"

// FILE FILTER CONSTANTS
FILTER_HTML     = "**/*.html";
FILTER_SASS     = "**/*.scss";
FILTER_CSS      = "**/*.css";
FILTER_JS       = "**/*.js";

// FILE EXTENSION CONSTANTS
EXTENSION_HTML  = "*.html";
EXTENSION_SASS  = "*.scss";
EXTENSION_CSS   = "*.css";
EXTENSION_JS    = "*.js";

// SOURCE CONSTANTS
SOURCE_SASS     = BASE_DIR + "/scss/**/" + EXTENSION_SASS;
SOURCE_JS       = BASE_DIR + "/js/**/" + EXTENSION_JS;
SOURCE_TEMPLATE = BASE_DIR + "/*.html";
SOURCE_CSS      = DEST_SASS = BASE_DIR + "/css";

// DESTINATION CONSTANTS
DEST_CSS        = SERVE_DIR + "/css";
DEST_JS         = SERVE_DIR + "/js";


// Process Bower Task
// --------------------------
gulp.task('bower', function(done){
  var jsFilter  = $.filter(FILTER_JS, {restore: true});
	var sassFilter = $.filter(FILTER_SASS, {restore: true});
  var cssFilter = $.filter(FILTER_CSS, {restore: true});

  return gulp.src(mainBowerFiles())
		.pipe(jsFilter)
		.pipe($.concat('vendor.js'))
    .pipe($.uglify({ preserveComments: 'license' }))
    .pipe(gulp.dest(DEST_JS))
		.pipe(jsFilter.restore)
		.pipe(sassFilter)
		.pipe($.sass())
    .pipe($.concat('vendor.css'))
    .pipe(sassFilter.restore)
    .pipe(cssFilter)
    .pipe($.concat('vendor.css'))
    .pipe($.cssnano())
    .pipe(gulp.dest(DEST_CSS))
		.pipe(cssFilter.restore);
    done();
});

// Styles Pre-processor Task
// This is needed so we can create a temp CSS
// folder so the index.html can read source CSS
// Needs refactor. Maybe change how multiple CSS/JS files
// are handled in the template.
// This method recompiles *.scss files twice on each change.
// --------------------------
gulp.task('sass', function(done){
  return gulp.src(SOURCE_SASS)
    .pipe($.changed(DEST_SASS, {extension: '.css'}))
    .pipe($.sass())
    .pipe(gulp.dest(DEST_SASS))
    done();
});

// Styles Task
// -----------
gulp.task('style', ['sass'], function(done){
  return gulp.src(SOURCE_SASS)
    .pipe($.sourcemaps.init())
    .pipe($.changed(SOURCE_SASS, {extension: FILTER_SASS}))
    .pipe($.sass())
    .pipe($.concat('main.min.css'))
    .pipe($.autoprefixer())
    .pipe($.if(isProductionMode, $.if(FILTER_CSS, $.cssnano())))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest(DEST_CSS));
    done();
});

// JS Task
// -------
gulp.task('js', function(done){
  return gulp.src(SOURCE_JS)
    .pipe($.changed(DEST_JS))
      .pipe($.sourcemaps.init())
      .pipe($.concat('main.min.js'))
      .pipe($.if(isProductionMode, $.if(FILTER_JS, $.uglify({ preserveComments: 'license' }))))
      .pipe($.sourcemaps.write())
    .pipe(gulp.dest(DEST_JS));
    done();
});

// Templates task
// --------------
gulp.task('templates', ['style'], function(done){
  var initSourceMap = lazypipe().pipe($.sourcemaps.init, { loadMaps: true });
  return gulp.src(SOURCE_TEMPLATE)
    .pipe($.useref({}, initSourceMap ))
    .pipe($.if(isProductionMode, $.if(FILTER_JS, $.uglify({ preserveComments: 'license' }))))
    .pipe($.if(FILTER_CSS, $.autoprefixer()))
    .pipe($.if(isProductionMode, $.if(FILTER_CSS, $.cssnano())))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest(SERVE_DIR));
    done();
});

// Cleanup task
// ------------
gulp.task('cleanup', function(done){
  del([SERVE_DIR]);
  del([SOURCE_CSS]);
  done();
});

// WATCH task
// ----------
gulp.task('watch', function(done){
  gulp.watch(SOURCE_SASS, ['style']);
  gulp.watch(SOURCE_JS, ['js']);
  gulp.watch(SOURCE_TEMPLATE, ['templates']);
  done();
});

// BrowserSync task
// ----------------
gulp.task('browserSync', ['templates'], function() {
  browserSync.init([SERVE_DIR], {
    server: { baseDir: SERVE_DIR }
  });
});

// DEFAULT Task
// ---------------------------------------
gulp.task('default', ['bower', 'cleanup', 'watch', 'browserSync']);
